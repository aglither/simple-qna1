# Simple QnA

간단한 1:1 질문&답변 기능



## License

제작자의 허가를 받았거나 출처를 표기한 경우에 한해

(위 사항을 어기지 않으면) 자유롭게 수정/배포/사용 가능합니다.

직·간접적인 어떤 종류의 모든 보증을 부인합니다.



## Getting Started

사용법



### Prerequisites

`PHP`, `MariaDB` 서버, 브라우저



### Setting

누구나 질문하고 누구나 답할 수 있게 되어 있습니다.

관리자만 답할 수 있게 하려면 `list.php`와 `a.php`를 적당히 바꿔주세요.



## Deployment

적당히 복붙하고 스타일을 바꿔주세요.



## Contributing

간단한 수정이라도 PR 환영합니다.

```C
if(cond)
{
    something();
}
```

줄바꿈 많이 쓰고, 인덴트는 Space 네 개로 해 주세요.



## Authors

- 맹주영 - _Initial work_ - [mjy9088](https://gitlab.com/mjy9088)



## Acknowledgment

- 기능 요청도 환영합니다!